//
//  MainViewController.swift
//  PrimerLogin
//
//  Created by Oleg Kleiman on 27/09/2019.
//  Copyright © 2019 Oleg Kleiman. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase

class MainViewController: UIViewController, LoginButtonDelegate {


    override func viewDidLoad() {
        super.viewDidLoad()

//        if (AccessToken.current != nil)
//        {
//            // User is already logged in, do work such as go to next view controller.
//        }
//        else {
        
            let loginView : FBLoginButton = FBLoginButton()
            view.addSubview(loginView)
            loginView.center = self.view.center
            loginView.permissions = ["public_profile", "email"]
//        }
        
    }

    // MARK: - LoginButtonDelegate
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if( error == nil ) {
            let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString )
        } else {
            print(error?.localizedDescription)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
    }

    @IBAction func facebookLogin(sender: AnyObject) {
        let loginManager: LoginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                print("Failed to login: \(error.localizedDescription)")
                return
            }
            
            guard let accessToken = AccessToken.current else {
              print("Failed to get access token")
              return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            Auth.auth().signInAndRetrieveData(with: credential) { (user, error) in
              if let error = error {
                print("Login error: \(error.localizedDescription)")
                let alertController = UIAlertController(title: "Login Error", message: error.localizedDescription, preferredStyle: .alert)
                let okayAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(okayAction)
                self.present(alertController, animated: true, completion: nil)
                return
              }
              // self.performSegue(withIdentifier: self.signInSegue, sender: nil)
            }
        }
    }

}
